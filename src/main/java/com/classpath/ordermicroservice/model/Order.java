package com.classpath.ordermicroservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor

@Entity
@Table(name="orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="name", nullable = false)
    @JsonProperty(value = "name")
    @NotEmpty(message = "customer name cannot be empty")
    private String customerName;
    @JsonProperty(value = "email")
    private String customerEmail;

    @Min(value = 25000, message = "order value cannot be less than 25k")
    @Max(value = 85000, message = "order value cannot be more than 80k")
    private double price;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @PastOrPresent(message = "Order date cannot be in the future")
    private LocalDate date;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonManagedReference
    private Set<LineItem> lineItems;

    //scaffolding code
    public void addLineItem(LineItem lineItem){
        if (this.lineItems == null){
            this.lineItems = new HashSet<>();
        }
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }
}
