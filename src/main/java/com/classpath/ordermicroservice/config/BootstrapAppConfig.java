package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.model.LineItem;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class BootstrapAppConfig {

    private final OrderRepository orderRepository;
    private final Faker faker = new Faker();

    @EventListener(value = ApplicationReadyEvent.class)
    public void bootstrapAppData(ApplicationReadyEvent event){
        IntStream.range(0,25000).forEach(index -> {
            Order order = Order.builder()
                                .customerEmail(faker.internet().emailAddress())
                                .customerName(faker.name().firstName())
                                .date(faker.date().past(10, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                                .price(faker.number().randomDouble(2, 25000, 50000))
                            .build();

            IntStream.range(0,4).forEach(value -> {
                LineItem lineItem = LineItem.builder()
                                        .name(faker.commerce().productName())
                                        .price(faker.number().randomDouble(2, 10000, 15000))
                                        .qty(faker.number().numberBetween(2, 5))
                        .build();
                order.addLineItem(lineItem);
            });
            this.orderRepository.save(order);
        });
    }
}
