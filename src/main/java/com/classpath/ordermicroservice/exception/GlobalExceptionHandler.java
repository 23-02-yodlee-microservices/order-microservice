package com.classpath.ordermicroservice.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
@Slf4j
@Configuration
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(NOT_FOUND)
    public Error handleInvalidOrderException(IllegalArgumentException exception){
      log.error("Exception whil processing the request :: {}", exception.getMessage());
      return new Error(200, exception.getMessage());
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    @ResponseStatus(NOT_FOUND)
    public Error handleInvalidOrderException(EmptyResultDataAccessException exception){
      log.error("Exception whil processing the request :: {}", exception.getMessage());
      return new Error(400, exception.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Set<String>> handleInvalidInputException(MethodArgumentNotValidException exception){
      log.error("Exception while processing the request :: ");
        List<ObjectError> allErrors = exception.getAllErrors();
        Set<String> errors = allErrors.stream().map(ObjectError::getDefaultMessage).collect(Collectors.toSet());
        Map<String, Set<String>> errorMap = new HashMap<>();
        errorMap.put("errors", errors);
        return errorMap;

    }
}

@RequiredArgsConstructor
@Getter
class Error{
    private final int code;
    private final String message;
}
